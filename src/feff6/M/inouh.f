      subroutine inouh (dps,dqs,drs,dq1s,dfls,dvs,zin,testi,
     $     nucs,nstopi,jc)
c
c initial values for the outward integration
c dps=large component;     dqs=small component;     drs=radial mesh
c dq1s=slope at the origin of dps or dqs;  dfls=power of the first term
c du=developpement limite;  dvs=potential at the first point
c zin=atomic number      testi=test of the precision
c finite nuclear size if nucs is non-zero
c nstopi controls the convergence  du developpement limite
c **********************************************************************
      implicit double precision (a-h,o-z)
      save
      include 'atom.h'
c
c dep,deq=derivatives of dps and dq; dd=energy/dvc;
c dvc=speed of light in a.u.;
c dsal=2.*dvc   dk=kappa quantum number
c dm=exponential step/720.
c **********************************************************************
cc      common /trois/ dpno(4,30), dqno(4,30)
      dimension dps(mepts), dqs(mepts), drs(mepts)
      do 10 i=1,10
      dps(i)=zero
   10 dqs(i)=zero
      if (nucs) 20,20,60
   20 dval=zin/dvc
      deva1=-dval
      deva2=dvs/dvc+dval/drs(1)-dd
      deva3=zero
      if (dk) 30,30,40
   30 dbe=(dk-dfls)/dval
      go to 50
   40 dbe=dval/(dk+dfls)
   50 dqs(10)=dq1s
      dps(10)=dbe*dq1s
      go to 90
 60   dval=dvs+zin*(3-drs(1)*drs(1)/(drs(nucs)*drs(nucs)))/
     $     (drs(nucs)+drs(nucs))
      deva1=zero
      deva2=(dval-3*zin/(drs(nucs)+drs(nucs)))/dvc-dd
      deva3=zin/(drs(nucs)*drs(nucs)*drs(nucs)*dsal)
      if (dk) 70,70,80
   70 dps(10)=dq1s
      go to 90
   80 dqs(10)=dq1s
   90 do 100 i=1,5
      dps(i)=dps(10)
      dqs(i)=dqs(10)
      dep(i)=dps(i)*dfls
  100 deq(i)=dqs(i)*dfls
      m=1
  110 dm=m+dfls
      dsum=dm*dm-dk*dk+deva1*deva1
      dqr=(dsal-deva2)*dqs(m+9)-deva3*dqs(m+7)
      dpr=deva2*dps(m+9)+deva3*dps(m+7)
      dval=((dm-dk)*dqr-deva1*dpr)/dsum
      dsum=((dm+dk)*dpr+deva1*dqr)/dsum
      j=-1
      do 130 i=1,5
      dpr=drs(i)**m
      dqr=dsum*dpr
      dpr=dval*dpr
      if (m.eq.1) go to 120
  120 dps(i)=dps(i)+dpr
      dqs(i)=dqs(i)+dqr
      if (abs(dpr/dps(i)).le.testi.and.abs(dqr/dqs(i)).le.testi) j=1
      dep(i)=dep(i)+dpr*dm
  130 deq(i)=deq(i)+dqr*dm
      if (j.eq.1) go to 140
      dps(m+10)=dval
      dqs(m+10)=dsum
      m=m+1
      if (m.le.20) go to 110
      nstopi=45
  140 do 150 i=1,4
      dpno(i,jc)=dps(i+9)
  150 dqno(i,jc)=dqs(i+9)
      return
      end
