      double precision function xx (j)
      include 'const.h'
      integer j
c     x grid point at index j, x = log(r), r=exp(x)
      xx = -c88 + c05*(j-1)
      return
      end

      double precision function rr(j)
      double precision xx
      integer j
      external xx
c     r grid point at index j
      rr = exp (xx(j))
      return
      end

      function ii(r)
c     index of grid point immediately below postion r
      include 'const.h'
      double precision r
      ii = (log(r) + c88) / c05 + 1
      return
      end
