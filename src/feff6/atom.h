      
      include 'mepts.h'
      include 'const.h'
     
      common /atomco/ den(30), dq1(30), dfl(30), ws, nqn(30), nql(30),
     1                nk(30), nmax(30), nel(30), norb, norbco
      common /dira/ dv(mepts), dr(mepts), dp(mepts), dq(mepts), 
     1     dpas, tets, z, nstop, nes, np, nuc
      common /deux/ dvn(mepts), dvf(mepts), d(mepts), dc(mepts), 
     1     dgc(mepts,30), dpc(mepts,30)
 

      common /trois/ dpno(4,30), dqno(4,30)

      common /ps1/ dep(5), deq(5), db, dvc, dsal, dk, dm
      common /ps2/ dexv, dexe, dcop, test, teste,
     1             testy, testv, niter, ion, icut, iprat, irnorm

      integer iprint
      common /print/ iprint

c titre = identification of the wave functions  s,p*,p,........
      character*40 ttl
      character*2  titre
      common /char/ titre(30), ttl
