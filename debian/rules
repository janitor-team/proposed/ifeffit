#!/usr/bin/make -f

#export DH_VERBOSE = 1

ifndef PERL
PERL = /usr/bin/perl
endif

config.status: configure
	dh_testdir
# Begin by saving some configuration files which may be outdated
	cp -a config.guess config.guess.save
	cp -a config.sub config.sub.save
# Copy newer versions of these configuration scripts over if they exist
ifneq "$(wildcard /usr/share/misc/config.sub)" ""
	cp -f /usr/share/misc/config.sub config.sub
endif
ifneq "$(wildcard /usr/share/misc/config.guess)" ""
	cp -f /usr/share/misc/config.guess config.guess
endif
	./configure --host=$(DEB_HOST_GNU_TYPE) \
	 --build=$(DEB_BUILD_GNU_TYPE) --prefix=/usr \
	 --mandir=\$${prefix}/share/man --infodir=\$${prefix}/share/info \
	 --with-pgplot-link="-lpgplot"

build: build-arch build-indep
build-arch: build-stamp
build-indep: build-stamp
build-stamp: config.status build-ifeffit build-perl
	touch $@

build-ifeffit:
	dh_testdir
	$(MAKE)
	touch $@

# Configure and build Perl extensions.  This must be done
# AFTER the main package is built
build-perl: build-ifeffit
	dh_testdir
# Here is the kludge to avoid the rpath lintian warning
#	cp debian/Makefile.PL wrappers/perl/
	cd wrappers/perl && \
	 $(PERL) Makefile.PL INSTALLDIRS=vendor && \
	 $(MAKE)
	touch $@

build-docs:
	dh_testdir
	mkdir tmp_docs
	cp -a examples tmp_docs
	find tmp_docs/examples -executable -type f -exec chmod 0644 {} \;
	rm -rf tmp_docs/examples/scripting
# Generate PDF documentation from postscript files.  Note that the
# postcript documentation needs to be refreshed occasionally using the
# 'get-docs' rule.  This avoids the need for the buildds to download them
# at build time and risk failing if they are not available.
	mkdir tmp_docs/pdf
	ps2pdf debian/feffit.ps tmp_docs/pdf/feffit.pdf ; \
	ps2pdf debian/autobk.ps tmp_docs/pdf/autobk.pdf ; \
	ps2pdf debian/diffkk.ps tmp_docs/pdf/diffkk.pdf ; \
	ps2pdf debian/feff6.ps tmp_docs/pdf/feff6.pdf ; \
	cd doc/Tutorial && make pdf && \
	 mv tutor.pdf ../../tmp_docs/pdf/ifeffit_tutorial.pdf
	cd doc/RefMan && make pdf && \
	 mv refman.pdf ../../tmp_docs/pdf/ifeffit_reference.pdf
	touch $@

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp build-ifeffit build-perl build-docs

	[ ! -f Makefile ] || $(MAKE) distclean

# Clean up any config leftovers in original tarball which have been
# regenerated during build so they don't appear in the diff.gz
	rm -f wrappers/perl/Makefile.PL
	rm -f wrappers/perl/Makefile
	rm -f wrappers/perl/pm_to_blib
	rm -f wrappers/python/site_install.py
	rm -f wrappers/tcl/TclSetup.in
	rm -f readline/config.log
	rm -f readline/config.status
	rm -f readline/stamp-h
	rm -f readline/config.h
	rm -f readline/Makefile
	rm -f readline/doc/Makefile
	rm -f readline/shlib/Makefile
	rm -f readline/examples/Makefile
	rm -f configure.in.save
# Restore saved configuration files if they exist to minimize diffs
ifneq "$(wildcard config.sub.save)" ""
	mv config.sub.save config.sub
endif
ifneq "$(wildcard config.guess.save)" ""
	mv config.guess.save config.guess
endif

# Clean up PDF downloaded documents
	rm -rf tmp_docs
	cd doc/Tutorial && make clean && rm -f *.bak
	cd doc/RefMan && make clean && rm -f *.bak

# Clean up Python extensions
	rm -rf wrappers/python/*.pyc
	rm -rf wrappers/python/build
	rm -rf build

# Clean up Perl extensions
	rm -rf wrappers/perl/blib
	rm -rf wrappers/perl/pm-to-blib
	rm -rf wrappers/perl/*.o
	rm -rf wrappers/perl/*.bs
	rm -rf wrappers/perl/Makefile

	dh_clean

install: build install-prereq install-ifeffit install-perl

install-prereq:
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs

install-ifeffit:
	$(MAKE) install prefix=$(CURDIR)/debian/ifeffit/usr

# Install Perl extensions into debian/libifeffit-perl.
install-perl:
# Strip RPATH from Ifeffit.so before installing
	chrpath -d wrappers/perl/blib/arch/auto/Ifeffit/Ifeffit.so
# old syntax, should be replaced as below...
#	cd wrappers/perl && $(MAKE) install \
#	 PREFIX=$(CURDIR)/debian/libifeffit-perl/usr INSTALLDIRS=vendor
	cd wrappers/perl && $(MAKE) install \
	 DESTDIR=$(CURDIR)/debian/libifeffit-perl/ INSTALLDIRS=vendor
# The Makefile in wrappers/perl seems to be malformed and does not
# install Ifeffit.pm in /usr/share/perl5.  We shall do it by hand ...
	dh_install --package=libifeffit-perl wrappers/perl/Ifeffit.pm \
	 usr/share/perl5/
# In fact, sometimes it puts them in /usr/lib/perl5 along with
# ifeffit_shell.pl and test_a.pl which we don't want for the pacakge.
# So get rid of 'em!
	rm -f $(CURDIR)/debian/libifeffit-perl/usr/lib/perl5/Ifeffit.pm
	rm -f $(CURDIR)/debian/libifeffit-perl/usr/lib/perl5/ifeffit_shell.pl
	rm -f $(CURDIR)/debian/libifeffit-perl/usr/lib/perl5/test_a.pl

install-docs: install-prereq

# Build architecture-independent files here.
binary-indep: build-docs install-docs
	dh_testdir -i
	dh_testroot -i
	dh_installdocs -i
	dh_installchangelogs ChangeLog -i
	dh_compress --exclude=.pdf --exclude=.iff --exclude=.dat \
	   --exclude=.chi --exclude=.xmu --exclude=.inp --exclude=.out \
	   --exclude=.sav --exclude=.py --exclude=LIST --exclude=AA -i
	dh_fixperms -i
	dh_installdeb -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir -a
	dh_testroot -a
	dh_installdocs -a
	dh_installexamples -a
	dh_installmenu -a
	dh_installman -a
	dh_installinfo -a
	dh_installchangelogs ChangeLog -a
	dh_perl --package=libifeffit-perl
	dh_link -a
	dh_strip -a
	dh_compress -a
	dh_fixperms -a
	dh_installdeb -a
	dh_shlibdeps -a
	dh_gencontrol -a
	dh_md5sums -a
	dh_builddeb -a

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install
